export interface NewUser {
    firstName?: string;
    lastName?: string;
    email?: string;
    birthDate: Date;

    teamId?: number;
}