import { Directive, ElementRef } from "@angular/core";

@Directive({
    selector: '[finished]'
})

export class FinishedDirective {
    constructor(private elementRef: ElementRef<HTMLDivElement>) {
        let marker = document.createElement('div');
        
        marker.style.height = '10px';
        marker.style.width = '10px';
        marker.style.borderRadius = '5px';
        marker.style.backgroundColor = 'green';
        marker.style.marginRight = '20px';
        marker.style.float = 'left';

        elementRef.nativeElement.insertAdjacentElement("beforeend", marker);
    }
}