import { Component, EventEmitter, Input, OnDestroy, Output } from "@angular/core";
import { Router } from "@angular/router";
import { Subject } from "rxjs";
import { HttpClientService } from "src/app/services/http-client.service";

@Component({
    selector: '',
    template: ''
})
export abstract class ItemComponent<T extends { id: number }> implements OnDestroy {
    @Input() public item: T;
    @Output() public onDeleteItem = new EventEmitter<number>();

    protected $unsubscribe = new Subject();

    constructor(
        protected httpClient: HttpClientService,
        protected router: Router
    ) {
        this.httpClient = httpClient;
        this.router = router;
    }

    public goToUpdateDialog(): void {
        this.router.navigate([this.router.url + '/' + this.item.id + '/update'], {
            queryParams: this.item
        });
    }

    public deleteItem(): void {
        this.onDeleteItem.emit(this.item.id);
    }

    ngOnDestroy(): void {
        this.$unsubscribe.next();
        this.$unsubscribe.complete();
    }
}