import { ActivatedRoute, Router } from "@angular/router";
import { map, switchMap, takeUntil } from "rxjs/operators";
import { Task } from "src/app/models/task/task";
import { Component, OnInit } from "@angular/core";
import { HttpClientService } from "src/app/services/http-client.service";
import { BaseListComponent } from "src/app/common/abstractions/base-list-component";

@Component({
    selector: 'app-list-tasks',
    templateUrl: './tasks-list.component.html',
    styleUrls: [
        './tasks-list.component.scss',
        './../../common/styles/item.scss'
    ]
})

export class TasksListComponent extends BaseListComponent<Task> implements OnInit {
    public projectId: number;

    constructor(
        httpClient: HttpClientService,
        router: Router,
        private route: ActivatedRoute
    ) {
        super(httpClient, router)

        this.httpClient.get<Task[]>('/Tasks')
            .pipe(
                map(arr => arr.filter(t => t.projectId === this.projectId)),
                takeUntil(this.$unsubscribe)
            )
            .subscribe((values: Task[]) => {
                console.log(values);
                this.itemsList = values;
            },
            (error: Error) => {
                console.warn(error);
            });
    }

    public deleteItem(itemId: number): void {
        this.httpClient.delete('/Tasks/' + itemId)
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe(() => {
                console.log('Success');

                let taskIndex = this.itemsList.findIndex((t) => t.id === itemId);
                this.itemsList.splice(taskIndex, 1);
            },
            (error: Error) => {
                console.warn(error);
            });
    }

    ngOnInit(): void {
        this.route.paramMap.pipe(
            switchMap(params => params.getAll('id'))
        )
        .subscribe((data: string) => this.projectId = +data);
    }
}