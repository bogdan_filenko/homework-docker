import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { takeUntil } from "rxjs/operators";
import { CRUDItemComponent } from "src/app/common/abstractions/crud-item-component";
import { updateProjectGroup } from "src/app/common/form-groups/projectGroups";
import { Project } from "src/app/models/project/project";
import { UpdateProject } from "src/app/models/project/update-project";
import { HttpClientService } from "src/app/services/http-client.service";

@Component({
    selector: 'app-update-project',
    templateUrl: 'update-project.component.html',
    styleUrls: [ './../../common/styles/crud.scss' ]
})

export class UpdateProjectComponent extends CRUDItemComponent implements OnInit {
    public updatableProject: UpdateProject;

    constructor(
        private readonly httpClient: HttpClientService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        super();
        this.formGroup = updateProjectGroup;
    }

    ngOnInit(): void {
        this.route.queryParams
            .subscribe((params: Params) => {
                this.updatableProject = {
                    id: params['id'],
                    name: params['name'],
                    description: params['description'],
                    deadline: params['deadline']
                }
        });
    }

    public updateProject(): void {
        this.httpClient.put<UpdateProject, Project>('/Projects', this.updatableProject)
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe(() => {
                this.isSaved = true;
                this.router.navigate(['/Projects']);
            },
            (error: Error) => (console.warn(error))
        );
    }

    public backToMainList(): void {
        this.router.navigate(['/Projects']);
    }
}