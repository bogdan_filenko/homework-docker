import { Router } from "@angular/router";
import { Component } from "@angular/core";
import { takeUntil } from "rxjs/operators";
import { User } from "src/app/models/user/user";
import { HttpClientService } from "src/app/services/http-client.service";
import { BaseListComponent } from "src/app/common/abstractions/base-list-component";

@Component({
    selector: 'app-list-users',
    templateUrl: './users-list.component.html',
    styleUrls: [
        './users-list.component.scss',
        './../../common/styles/item.scss'
    ]
})

export class UsersListComponent extends BaseListComponent<User> {

    constructor(
        httpClient: HttpClientService,
        router: Router
    ) {
        super(httpClient, router);

        this.httpClient.get<User[]>(router.url)
            .pipe(takeUntil(this.$unsubscribe))
            .subscribe((values: User[]) => {
                this.itemsList = values;
            },
            (error: Error) => {
                console.warn(error);
            });
    }
}