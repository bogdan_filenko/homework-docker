import { Component } from "@angular/core";
import { Project } from "src/app/models/project/project";
import { ItemComponent } from "src/app/common/abstractions/item-component";
import { HttpClientService } from "src/app/services/http-client.service";
import { Router } from "@angular/router";

@Component({
    selector: 'app-project',
    templateUrl: './project.component.html',
    styleUrls: [
        './project.component.scss',
        './../../common/styles/item-content.scss'
    ]
})

export class ProjectComponent extends ItemComponent<Project> {
    constructor(
        public httpClient: HttpClientService,
        public router: Router
    ) {
        super(httpClient, router);
    }

    public goToTasksList(): void {
        this.router.navigate(['/Projects/' + this.item.id + '/Tasks']);
    }
}