using System;

#nullable enable

namespace Homework.Common.DTOs.User
{
    public sealed class CreateUserDTO
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTime BirthDate { get; set; }

        public int? TeamId { get; set; }
    }
}