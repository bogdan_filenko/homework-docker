using System;

#nullable enable

namespace Homework.Common.DTOs.Linq
{
    public sealed class TaskLinqDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public int State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public ProjectLinqDTO Project { get; set; }
        public UserLinqDTO Performer { get; set; }

        public override string ToString()
        {
            return  $"Id: {this.Id}\n" +
                    $"Name: {this.Name}\n" +
                    $"Description: {this.Description}\n" +
                    $"Created at: {this.CreatedAt}\n" +
                    $"Finished at: {(this.FinishedAt == default ? "Still in process" : this.FinishedAt.ToString())}\n" +
                    $"Performer: {this.Performer.FirstName + " " + this.Performer.LastName}";
        }
    }
}