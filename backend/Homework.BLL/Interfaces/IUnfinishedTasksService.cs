using System.Threading.Tasks;
using Homework.Common.DTOs.Task;
using System.Collections.Generic;

namespace Homework.BLL.Interfaces
{
    public interface IUnfinishedTasksService
    {
        Task<IEnumerable<TaskDTO>> GetUnfinishedTasks(int userId);
    }
}