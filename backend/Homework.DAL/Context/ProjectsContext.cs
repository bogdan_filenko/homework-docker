using Homework.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Homework.DAL.Context
{
    public sealed class ProjectsContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; } 
        public ProjectsContext(DbContextOptions<ProjectsContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.CreateModel();
            modelBuilder.Seed();
        }
    }
}