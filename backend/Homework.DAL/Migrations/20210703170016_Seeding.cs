﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Homework.DAL.Migrations
{
    public partial class Seeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 7, 3, 20, 0, 15, 417, DateTimeKind.Local).AddTicks(5734), "Dynamic Marketing Consultant" },
                    { 2, new DateTime(2021, 7, 3, 20, 0, 15, 423, DateTimeKind.Local).AddTicks(6917), "Human Functionality Planner" },
                    { 3, new DateTime(2021, 7, 3, 20, 0, 15, 423, DateTimeKind.Local).AddTicks(7066), "Central Optimization Strategist" },
                    { 4, new DateTime(2021, 7, 3, 20, 0, 15, 423, DateTimeKind.Local).AddTicks(7123), "Global Creative Supervisor" },
                    { 5, new DateTime(2021, 7, 3, 20, 0, 15, 423, DateTimeKind.Local).AddTicks(7167), "National Security Officer" },
                    { 6, new DateTime(2021, 7, 3, 20, 0, 15, 423, DateTimeKind.Local).AddTicks(7213), "International Paradigm Developer" },
                    { 7, new DateTime(2021, 7, 3, 20, 0, 15, 423, DateTimeKind.Local).AddTicks(7252), "Central Program Agent" },
                    { 8, new DateTime(2021, 7, 3, 20, 0, 15, 423, DateTimeKind.Local).AddTicks(7290), "Chief Brand Officer" },
                    { 9, new DateTime(2021, 7, 3, 20, 0, 15, 423, DateTimeKind.Local).AddTicks(7331), "Chief Optimization Assistant" },
                    { 10, new DateTime(2021, 7, 3, 20, 0, 15, 423, DateTimeKind.Local).AddTicks(7371), "National Operations Administrator" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDate", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 5, new DateTime(2014, 3, 15, 12, 29, 57, 100, DateTimeKind.Unspecified).AddTicks(3920), "Abbigail_Hackett58@yahoo.com", "Janick", "Kreiger", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(3637), 1 },
                    { 14, new DateTime(2002, 6, 8, 9, 9, 4, 128, DateTimeKind.Unspecified).AddTicks(4661), "Billy74@gmail.com", "Luz", "Gusikowski", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(5770), 10 },
                    { 3, new DateTime(2009, 7, 5, 13, 2, 36, 345, DateTimeKind.Unspecified).AddTicks(8512), "Noe7@gmail.com", "Madalyn", "Schiller", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(3068), 10 },
                    { 21, new DateTime(2007, 3, 23, 17, 15, 26, 651, DateTimeKind.Unspecified).AddTicks(3830), "Zack.Gerlach@gmail.com", "Brielle", "Pouros", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(7097), 9 },
                    { 18, new DateTime(2011, 6, 20, 20, 45, 11, 149, DateTimeKind.Unspecified).AddTicks(2132), "Wilfrid92@yahoo.com", "Mateo", "Bartell", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(6528), 9 },
                    { 17, new DateTime(2004, 12, 13, 7, 52, 19, 755, DateTimeKind.Unspecified).AddTicks(5707), "Jett.Lowe85@yahoo.com", "Cletus", "Wilkinson", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(6351), 9 },
                    { 2, new DateTime(2002, 8, 26, 11, 53, 7, 844, DateTimeKind.Unspecified).AddTicks(9028), "Adan.Bartell82@hotmail.com", "Frank", "Halvorson", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(2488), 8 },
                    { 1, new DateTime(2007, 5, 1, 22, 14, 10, 495, DateTimeKind.Unspecified).AddTicks(5516), "Deanna.Nader30@yahoo.com", "Alisha", "Wisozk", new DateTime(2021, 7, 3, 20, 0, 15, 450, DateTimeKind.Local).AddTicks(4079), 8 },
                    { 23, new DateTime(2004, 2, 6, 19, 6, 5, 185, DateTimeKind.Unspecified).AddTicks(4448), "Lupe.Johnston30@yahoo.com", "Christop", "Schaefer", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(7500), 7 },
                    { 8, new DateTime(2003, 5, 16, 12, 18, 22, 843, DateTimeKind.Unspecified).AddTicks(6867), "Cordelia_Corkery@gmail.com", "Icie", "Lehner", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(4409), 7 },
                    { 12, new DateTime(2007, 5, 23, 21, 40, 7, 710, DateTimeKind.Unspecified).AddTicks(1713), "Lexi.Reichert76@gmail.com", "Winona", "Schowalter", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(5409), 6 },
                    { 11, new DateTime(2008, 6, 28, 18, 56, 35, 951, DateTimeKind.Unspecified).AddTicks(1150), "Lulu.Reynolds@hotmail.com", "Richie", "Bins", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(5160), 6 },
                    { 9, new DateTime(2014, 9, 15, 23, 32, 57, 176, DateTimeKind.Unspecified).AddTicks(8964), "Lonzo41@gmail.com", "Jazmin", "Johnston", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(4622), 6 },
                    { 6, new DateTime(2012, 10, 31, 11, 44, 32, 604, DateTimeKind.Unspecified).AddTicks(3660), "Wava_Grimes24@gmail.com", "Carmela", "Auer", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(3913), 6 },
                    { 28, new DateTime(2008, 6, 1, 2, 41, 9, 694, DateTimeKind.Unspecified).AddTicks(6085), "Virgie18@yahoo.com", "Vesta", "Greenfelder", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(8510), 5 },
                    { 15, new DateTime(2004, 3, 7, 21, 38, 21, 90, DateTimeKind.Unspecified).AddTicks(5926), "Gayle.Sporer@hotmail.com", "Rogelio", "Pfannerstill", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(5948), 5 },
                    { 20, new DateTime(2009, 12, 8, 11, 35, 34, 157, DateTimeKind.Unspecified).AddTicks(6384), "Fausto.Casper@yahoo.com", "Rico", "Lind", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(6923), 4 },
                    { 7, new DateTime(2005, 4, 5, 21, 14, 19, 190, DateTimeKind.Unspecified).AddTicks(4668), "Kale_Haley@yahoo.com", "Al", "Herman", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(4104), 4 },
                    { 25, new DateTime(2007, 9, 15, 10, 37, 20, 524, DateTimeKind.Unspecified).AddTicks(14), "Elvie86@hotmail.com", "Shyann", "Bogisich", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(7917), 3 },
                    { 22, new DateTime(2002, 8, 17, 14, 48, 39, 32, DateTimeKind.Unspecified).AddTicks(25), "Karl_Tillman94@yahoo.com", "Bettye", "Schultz", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(7330), 3 },
                    { 24, new DateTime(2011, 6, 28, 6, 28, 7, 641, DateTimeKind.Unspecified).AddTicks(6717), "Kaley.McDermott@yahoo.com", "Deron", "Kemmer", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(7738), 2 },
                    { 16, new DateTime(2011, 3, 4, 11, 28, 43, 845, DateTimeKind.Unspecified).AddTicks(2791), "Mariela_Koss@yahoo.com", "Christophe", "Boyer", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(6174), 2 },
                    { 13, new DateTime(2007, 8, 17, 14, 45, 23, 339, DateTimeKind.Unspecified).AddTicks(8834), "Billie18@yahoo.com", "Gilberto", "Adams", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(5598), 2 },
                    { 10, new DateTime(2003, 3, 23, 0, 6, 44, 474, DateTimeKind.Unspecified).AddTicks(887), "Pauline8@gmail.com", "Princess", "Schumm", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(4925), 2 },
                    { 4, new DateTime(2010, 11, 23, 9, 47, 57, 114, DateTimeKind.Unspecified).AddTicks(4852), "William_Lynch@gmail.com", "Nelda", "Ryan", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(3406), 2 },
                    { 30, new DateTime(2002, 10, 13, 20, 27, 27, 777, DateTimeKind.Unspecified).AddTicks(7928), "Alexis86@gmail.com", "Trace", "Sanford", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(8860), 1 },
                    { 29, new DateTime(2002, 5, 14, 11, 12, 49, 246, DateTimeKind.Unspecified).AddTicks(710), "Ines.Breitenberg@yahoo.com", "Foster", "Kautzer", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(8694), 1 },
                    { 26, new DateTime(2009, 8, 5, 23, 33, 25, 76, DateTimeKind.Unspecified).AddTicks(6848), "Chandler.Kuphal55@yahoo.com", "Madelynn", "Runolfsson", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(8100), 1 },
                    { 19, new DateTime(2010, 10, 21, 15, 30, 53, 303, DateTimeKind.Unspecified).AddTicks(9546), "Mark_Cummings33@gmail.com", "Adell", "Cruickshank", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(6700), 10 },
                    { 27, new DateTime(2004, 12, 23, 13, 38, 8, 605, DateTimeKind.Unspecified).AddTicks(170), "Elena_Haley68@hotmail.com", "Sabina", "Brekke", new DateTime(2021, 7, 3, 20, 0, 15, 465, DateTimeKind.Local).AddTicks(8274), 10 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 4, 29, new DateTime(2021, 7, 3, 20, 0, 15, 479, DateTimeKind.Local).AddTicks(1151), new DateTime(2021, 10, 20, 21, 28, 27, 228, DateTimeKind.Local).AddTicks(2098), "copy encompassing Norwegian Krone", "International Accountability Representative", 2 },
                    { 6, 16, new DateTime(2021, 7, 3, 20, 0, 15, 479, DateTimeKind.Local).AddTicks(3182), new DateTime(2023, 2, 18, 21, 53, 25, 684, DateTimeKind.Local).AddTicks(956), "Ranch white", "Customer Markets Executive", 2 },
                    { 7, 16, new DateTime(2021, 7, 3, 20, 0, 15, 479, DateTimeKind.Local).AddTicks(8659), new DateTime(2022, 7, 3, 8, 54, 13, 593, DateTimeKind.Local).AddTicks(4076), "reintermediate integrated Incredible Metal Chips", "Legacy Markets Technician", 1 },
                    { 10, 16, new DateTime(2021, 7, 3, 20, 0, 15, 480, DateTimeKind.Local).AddTicks(875), new DateTime(2022, 5, 17, 14, 10, 11, 397, DateTimeKind.Local).AddTicks(4282), "expedite SCSI", "Product Accounts Consultant", 6 },
                    { 9, 22, new DateTime(2021, 7, 3, 20, 0, 15, 479, DateTimeKind.Local).AddTicks(9564), new DateTime(2022, 12, 19, 1, 59, 30, 726, DateTimeKind.Local).AddTicks(9216), "Regional copy", "Dynamic Mobility Supervisor", 2 },
                    { 8, 7, new DateTime(2021, 7, 3, 20, 0, 15, 479, DateTimeKind.Local).AddTicks(9369), new DateTime(2022, 5, 11, 8, 42, 22, 696, DateTimeKind.Local).AddTicks(5490), "Designer Handmade", "National Accountability Engineer", 4 },
                    { 5, 6, new DateTime(2021, 7, 3, 20, 0, 15, 479, DateTimeKind.Local).AddTicks(1443), new DateTime(2021, 11, 6, 16, 43, 19, 265, DateTimeKind.Local).AddTicks(1391), "bifurcated Turnpike", "Forward Applications Planner", 8 },
                    { 1, 21, new DateTime(2021, 7, 3, 20, 0, 15, 476, DateTimeKind.Local).AddTicks(4470), new DateTime(2022, 3, 26, 20, 48, 37, 989, DateTimeKind.Local).AddTicks(5734), "New York Heights", "Forward Identity Planner", 2 },
                    { 2, 21, new DateTime(2021, 7, 3, 20, 0, 15, 477, DateTimeKind.Local).AddTicks(9195), new DateTime(2022, 10, 21, 10, 10, 27, 814, DateTimeKind.Local).AddTicks(4908), "Intelligent Money Market Account invoice", "Regional Tactics Orchestrator", 10 },
                    { 3, 27, new DateTime(2021, 7, 3, 20, 0, 15, 478, DateTimeKind.Local).AddTicks(1698), new DateTime(2022, 4, 14, 16, 33, 23, 670, DateTimeKind.Local).AddTicks(118), "Product California panel", "Central Mobility Architect", 2 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 7, 3, 20, 0, 15, 489, DateTimeKind.Local).AddTicks(8914), "Phased Tasty Rubber Cheese", null, "Incredible Plastic Chips Intelligent Frozen Tuna", 10, 4, 3 },
                    { 10, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(2946), "Intelligent Fresh Pants clicks-and-mortar", null, "secured line Implementation", 25, 9, 1 },
                    { 46, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(2048), "Identity", null, "Granite synthesize", 25, 9, 0 },
                    { 14, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(3509), "Refined", new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(3521), "Soft purple", 28, 8, 2 },
                    { 22, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(7088), "seize", null, "process improvement Forge", 24, 8, 1 },
                    { 41, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(986), "Refined Soft Salad Massachusetts indexing", null, "invoice extranet", 11, 8, 1 },
                    { 2, new DateTime(2021, 7, 3, 20, 0, 15, 491, DateTimeKind.Local).AddTicks(6230), "Mall ADP", null, "teal virtual", 26, 5, 0 },
                    { 7, new DateTime(2021, 7, 3, 20, 0, 15, 492, DateTimeKind.Local).AddTicks(1007), "Interactions", null, "implement Euro", 28, 5, 1 },
                    { 16, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(4644), "visualize Bolivia", null, "Tasty Cotton Cheese Investment Account", 29, 5, 3 },
                    { 30, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(8245), "Assistant Associate Fords", null, "Nicaragua uniform", 19, 5, 1 },
                    { 48, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(2293), "Pennsylvania navigate Tugrik", null, "Orchestrator multimedia", 16, 5, 0 },
                    { 12, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(3245), "transform Handmade", null, "synthesize Estonia", 9, 1, 0 },
                    { 19, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(5300), "gold Architect plug-and-play", new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(5312), "New Zealand sensor", 24, 1, 2 },
                    { 26, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(7689), "dedicated Intelligent Concrete Hat Saint Helena", new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(7700), "digital empowering", 26, 1, 2 },
                    { 37, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(476), "Human Kids extensible", null, "Minnesota turquoise", 25, 1, 0 },
                    { 45, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(1937), "Agent", null, "Markets Bedfordshire", 22, 1, 1 },
                    { 4, new DateTime(2021, 7, 3, 20, 0, 15, 491, DateTimeKind.Local).AddTicks(8194), "XML Idaho", null, "Nepalese Rupee solid state", 22, 2, 1 },
                    { 42, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(1092), "eco-centric Liaison", new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(1102), "Cotton Avon", 15, 2, 2 },
                    { 11, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(3124), "primary", null, "Sleek Practical Plastic Soap", 14, 3, 0 },
                    { 15, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(4406), "Tasty", new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(4434), "Factors initiative", 24, 3, 2 },
                    { 23, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(7242), "Small capacitor Sleek", null, "Creative Unbranded Metal Sausages", 1, 3, 0 },
                    { 28, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(8016), "Unbranded Wooden Fish Pataca Clothing", null, "client-server View", 7, 3, 3 },
                    { 8, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(1145), "Industrial Games & Books e-markets", null, "haptic Accounts", 28, 9, 1 },
                    { 47, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(2170), "visualize Tasty Wooden Computer", null, "Marketing SMTP", 28, 10, 1 },
                    { 44, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(1739), "back up Toys & Movies", null, "Cotton bandwidth", 17, 10, 0 },
                    { 36, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(355), "end-to-end", null, "Generic Croatian Kuna", 18, 10, 0 },
                    { 17, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(4836), "global", null, "Oklahoma Intelligent Plastic Chair", 26, 4, 0 },
                    { 18, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(5151), "artificial intelligence ivory", null, "payment blockchains", 14, 4, 0 },
                    { 24, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(7414), "Vermont", null, "Concrete Awesome Plastic Table", 9, 4, 3 },
                    { 27, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(7859), "Indiana", null, "Keys Steel", 6, 4, 1 },
                    { 29, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(8124), "Forward", new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(8134), "Port Future-proofed", 4, 4, 2 },
                    { 38, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(576), "Puerto Rico", new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(587), "Nebraska Mobility", 28, 4, 2 },
                    { 3, new DateTime(2021, 7, 3, 20, 0, 15, 491, DateTimeKind.Local).AddTicks(6546), "deposit Dynamic Kentucky", null, "West Virginia Savings Account", 15, 6, 0 },
                    { 6, new DateTime(2021, 7, 3, 20, 0, 15, 492, DateTimeKind.Local).AddTicks(359), "regional Fresh HTTP", null, "Lebanese Pound Metal", 18, 6, 1 },
                    { 25, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(7558), "Handmade Steel Fish Web Realigned", null, "Supervisor niches", 29, 6, 0 },
                    { 35, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(238), "firewall seize", new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(249), "intangible withdrawal", 16, 6, 2 },
                    { 34, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(115), "Representative neural Interactions", new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(127), "Union Distributed", 4, 3, 2 },
                    { 43, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(1205), "Lead parse", null, "panel Valley", 20, 6, 3 },
                    { 50, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(2501), "THX generate", null, "Forward policy", 21, 6, 1 },
                    { 9, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(1349), "logistical", null, "Auto Loan Account Savings Account", 29, 7, 1 },
                    { 21, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(6839), "harness", new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(6869), "Forward compressing", 22, 7, 2 },
                    { 31, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(8356), "Cotton Balanced", null, "Auto Loan Account encryption", 22, 7, 1 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 32, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(9686), "Identity Berkshire", null, "Fresh Gorgeous Soft Gloves", 2, 7, 3 },
                    { 33, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(9978), "Rustic Rubber Fish Path Avon", null, "Ridges Serbian Dinar", 9, 7, 0 },
                    { 40, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(853), "Universal turquoise", null, "silver Investment Account", 16, 7, 1 },
                    { 5, new DateTime(2021, 7, 3, 20, 0, 15, 491, DateTimeKind.Local).AddTicks(9632), "digital synthesize payment", null, "input Finland", 4, 10, 3 },
                    { 13, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(3405), "Via", null, "parse Reduced", 25, 10, 1 },
                    { 20, new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(5422), "foreground wireless", new DateTime(2021, 7, 3, 20, 0, 15, 493, DateTimeKind.Local).AddTicks(5434), "client-server South Dakota", 22, 10, 2 },
                    { 49, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(2398), "Rubber", null, "Metical gold", 25, 6, 3 },
                    { 39, new DateTime(2021, 7, 3, 20, 0, 15, 494, DateTimeKind.Local).AddTicks(744), "cohesive magenta", null, "scalable Ridge", 9, 3, 3 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10);
        }
    }
}
